#Nome: Eike Souza NUSP: 4618653
#miniEP09
using Test
using LinearAlgebra
function testmatrix()
    @test matriz_pot([1 2; 3 4], 1) == [1 2; 3 4]
    @test matriz_pot([1 2; 3 4], 2) == [7.0 10.0; 15.0 22.0]
    @test matriz_pot([0 0; 0 0], 1) == [0 0; 0 0]
    @test matriz_pot([6 12; 7 4], 2) == [120.0 120.0; 70.0 100.0]
    @test matriz_pot_by_squaring([1 2; 3 4], 1) == [1 2; 3 4]
    @test matriz_pot_by_squaring([1 2; 3 4], 2) == [7.0 10.0; 15.0 22.0]
    @test matriz_pot_by_squaring([0 0; 0 0], 1) == [0 0; 0 0]
    @test matriz_pot_by_squaring([6 12; 7 4], 2) == [120.0 120.0; 70.0 100.0]
    println("Fim dos testes!")
end

function multiplica(a, b)
    dima = size(a)
    dimb = size(b)
    if dima[2] != dimb[1]
        return -1
    end
    c = zeros(dima[1], dimb[2])
    for i in 1:dima[1]
        for j in 1:dimb[2]
            for k in 1:dima[2]
                c[i, j] = c[i, j] + a[i, k] * b[k, j]
            end
        end
    end
    return c
end

function matriz_pot(M, p)
    n = M
    if p == 1
        return M
    else
        while p > 1
            n = multiplica(n, M)
            p = p - 1
        end
        return n
    end
end

function matrix_pot_by_squaring(M, p)
    if p == 1
        return M
    end
    a = matrix_pot_by_squaring(M, div(p, 2))
    if p % 2 == 0
        return multiplica(a, a)
    else
        b = multiplica(a, a)
        return multiplica(M, b)
    end
end

function compare_times()
    m = [6 12; 7 4]
    @time matriz_pot(m, 2)
    @time matrix_pot_by_squaring(m, 2)
end
testmatrix()
compare_times()


